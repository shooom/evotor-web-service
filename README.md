# evotor-web-service

* Based on Spring Boot 2
* Test on PostgreSQL

## How to run

* clone repo
* create *evotor-service* database and user for it
* run with command *mvn package && java -jar target/evotor-web-service-0.0.1-SNAPSHOT.jar*

### example :
* create client: 
    *curl -d '{"type": "create", "login": "evouser", "password": "123qwe"}' -H "Content-Type: application/json" -X POST http://localhost:8080/message*

* get balance: *curl -d '{"type": "get-balance", "login": "evouser", "password": "123qwe"}' -H "Content-Type: application/json" -X POST http://localhost:8080/message*

## TODO:
* add logger
* run in docker container
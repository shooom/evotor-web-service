DROP TABLE IF EXISTS client;
CREATE TABLE client(
    id serial PRIMARY KEY,
    login VARCHAR(100) NOT NULL UNIQUE,
    password VARCHAR(100),
    balance DECIMAL
);
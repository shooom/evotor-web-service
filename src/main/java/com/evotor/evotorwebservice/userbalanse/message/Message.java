package com.evotor.evotorwebservice.userbalanse.message;

public class Message {

	public static final String CREATE_TYPE = "create";
	public static final String GET_BALANCE_TYPE = "get-balance";

    private final String type;
    private final String login;
    private final String password;

    public Message(String type, String login, String password) {
        this.type = type;
        this.login = login;
        this.password = password;
    }

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
    }
    
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	@Override
	public String toString() {
		return String.format("Message login = %s pass = %s type = %s", login, password, type);
	}

}
package com.evotor.evotorwebservice.userbalanse.message;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class ResponseMessage {

    public static int OK = 0;
    public static int USER_ALREADY_EXIST = 1;
    public static int SERVER_ERROR = 2;
    public static int USER_NOT_FOUND = 3;
	public static int PASSWORD_IS_NOT_CORRECT = 4;
	
	class BalanceResult {
		private BigDecimal balance;

		BalanceResult(BigDecimal balance) {
			this.setBalance(balance);
		}

		/**
		 * @return the balance
		 */
		public BigDecimal getBalance() {
			return balance;
		}

		/**
		 * @param balance the balance to set
		 */
		public void setBalance(BigDecimal balance) {
			this.balance = balance;
		}

	}

	private int result;

	@JsonInclude(Include.NON_NULL)
    private BalanceResult extras;

    public ResponseMessage(int result) {
		this.setResult(result);
    }

    public ResponseMessage(int result, BigDecimal extras) {
        this.setResult(result);
        this.setExtras(extras);
    }

    /**
	 * @return the extras
	 */
	public BalanceResult getExtras() {
		return extras;
	}

	/**
	 * @param extras the extras to set
	 */
	public void setExtras(BigDecimal extras) {
		this.extras = this.new BalanceResult(extras);
	}

	/**
	 * @return the result
	 */
	public int getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(int result) {
		this.result = result;
	}
}
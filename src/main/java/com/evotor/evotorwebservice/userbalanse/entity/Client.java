package com.evotor.evotorwebservice.userbalanse.entity;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	name="client",
	uniqueConstraints = @UniqueConstraint(columnNames={"login"})
)
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String login;
    private String password;
    private BigDecimal balance;

    Client() {
        super();
    }

    public Client(String login, String password) {
        this.login = login;
        this.password = password;
        this.balance = new BigDecimal(0);
    }

    public long getId() {
        return id;
    }

	/**
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}
	/**
	 * @param login the login to set
	 */
	public void setLogin(String login) {
		this.login = login;
    }
    
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
    }
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
    }

	/**
	 * @return the balance
	 */
	public BigDecimal getBalance() {
		return balance;
	}
	/**
	 * @param balance the balance to set
	 */
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

    @Override
    public String toString() {
        return "login: " + this.login + ", balance: " + this.balance;
    }
}
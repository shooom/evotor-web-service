package com.evotor.evotorwebservice.userbalanse.repository;

import com.evotor.evotorwebservice.userbalanse.entity.Client;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
    Client findByLogin(@Param("login") String login);
}

package com.evotor.evotorwebservice.userbalanse.controller;

import org.springframework.web.bind.annotation.RestController;

import com.evotor.evotorwebservice.userbalanse.service.ClientService;
import com.evotor.evotorwebservice.userbalanse.message.Message;
import com.evotor.evotorwebservice.userbalanse.message.ResponseMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class UserBalanceController {

    @Autowired
    ClientService userService;
    
    @PostMapping("/message")
    public ResponseMessage messageHandler(@RequestBody Message msg) {
        ResponseMessage rMessage = null;
        
        try {
            switch(msg.getType()) {
                case Message.CREATE_TYPE:
                    rMessage = userService.addClient(msg);
                    break;
                case Message.GET_BALANCE_TYPE:
                    rMessage = userService.getBalance(msg);
                    break;
                default:
                    throw new RuntimeException("Type of message is not valid");
            }
        } catch(Exception e) {
            rMessage = new ResponseMessage(ResponseMessage.SERVER_ERROR);
        }
        return rMessage;
    }
}

package com.evotor.evotorwebservice.userbalanse.service;

import com.evotor.evotorwebservice.userbalanse.message.Message;
import com.evotor.evotorwebservice.userbalanse.message.ResponseMessage;
import com.evotor.evotorwebservice.userbalanse.entity.Client;
import com.evotor.evotorwebservice.userbalanse.repository.ClientRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClientService {
    
    @Autowired
    ClientRepository repository;

    public ResponseMessage addClient(Message msg) {
        ResponseMessage rMessage = null;

        if (repository.findByLogin(msg.getLogin()) != null) {
            rMessage = new ResponseMessage(ResponseMessage.USER_ALREADY_EXIST);
        } else {
            repository.save(new Client(msg.getLogin(), msg.getPassword()));
            rMessage = new ResponseMessage(ResponseMessage.OK);
        }

        return rMessage;
    }

    public ResponseMessage getBalance(Message msg) {
        ResponseMessage rMessage = null;
        Client cln = repository.findByLogin(msg.getLogin());

        if (cln == null) {
            rMessage = new ResponseMessage(ResponseMessage.USER_NOT_FOUND);
        } else {
            if (!cln.getPassword().equals(msg.getPassword())) {
                System.out.println("Password is not correct");
                rMessage = new ResponseMessage(ResponseMessage.USER_NOT_FOUND);
            } else {
                rMessage = new ResponseMessage(ResponseMessage.OK, cln.getBalance());
            }
        }

        return rMessage;
    }
}